export const environment = {
  apiUrl: 'https://localhost:44390/api',
  apiUrlExternal: 'https://api.toka.com.mx/candidato/api/',
  production: true
};
