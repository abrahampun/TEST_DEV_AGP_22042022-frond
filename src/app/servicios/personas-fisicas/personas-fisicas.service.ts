import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { DataReporte } from 'src/app/Modelos/DataReporte';
import { PersonaFisica } from 'src/app/Modelos/PersonaFisica/PersonaFisica';
import { PersonaFisicaActualizar } from 'src/app/Modelos/PersonaFisica/PersonFisicaActualizar';
import { PersonaFisicaCrear } from 'src/app/Modelos/PersonaFisica/PersonFisicaCrear';
import { RespuestaErrorValidacionGenerica, RespuestaGenerica } from 'src/app/Modelos/RespuestaGenerica';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PersonasFisicasService {

  private url = environment.apiUrl;
  private urlExterno = environment.apiUrlExternal;
  constructor(private http: HttpClient) { }
  ObtenerPorId(id:number): Observable<PersonaFisica> {
    return this.http.get<PersonaFisica >(`${this.url + "/personafisica/" + id }`);
  }
  ObtenerTodo(): Observable<PersonaFisica[]> {
    return this.http.get<PersonaFisica[] >(`${this.url + "/personafisica/"}`);
  }
  Guardar(body:PersonaFisicaCrear): Observable<RespuestaGenerica  > {
    return this.http.post<RespuestaGenerica >(`${this.url + "/personafisica"}`,body);
  }
  Actualizar(body:PersonaFisicaActualizar): Observable<RespuestaGenerica  > {
    return this.http.put<RespuestaGenerica >(`${this.url + "/personafisica"}`,body);
  }
  Borrar(id:number): Observable<RespuestaGenerica> {
    return this.http.delete<RespuestaGenerica >(`${this.url + "/personafisica/" + id }`);
  }
  ObtenerExterno(token:string): Observable<DataReporte> {

    const headers = { 'Authorization': `Bearer ${token}`}  
    return this.http.get<DataReporte>(`${this.urlExterno + "customers"}`  ,{ headers:headers});
  }
}
