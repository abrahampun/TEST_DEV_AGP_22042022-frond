import { TestBed } from '@angular/core/testing';

import { PersonasFisicasService } from './personas-fisicas.service';

describe('PersonasFisicasService', () => {
  let service: PersonasFisicasService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PersonasFisicasService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
