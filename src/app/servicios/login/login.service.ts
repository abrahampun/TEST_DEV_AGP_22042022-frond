import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';
import { loginAuth } from 'src/app/Modelos/loginAuth';
import { loginData } from 'src/app/Modelos/loginData';
import { PersonaFisica } from 'src/app/Modelos/PersonaFisica/PersonaFisica';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient, private cookiesServicio: CookieService) { }
  private url = environment.apiUrlExternal;


  login(body:loginAuth): Observable<loginData> {
    return this.http.post<loginData >(`${this.url + "/login/authenticate"}`,body);
  }
}
