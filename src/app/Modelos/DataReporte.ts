
export interface DataReporte {
    Data:            DataContenido[];

}
export interface DataContenido {
    IdCliente:            number;
    FechaRegistroEmpresa: Date;
    RazonSocial:          string;
    RFC:                  string;
    Sucursal:             string;
    IdEmpleado:           number;
    Nombre:               string;
    Paterno:              string;
    Materno:              string;
    IdViaje:              number;
}