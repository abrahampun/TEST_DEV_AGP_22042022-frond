export interface PersonaFisicaCrear {
    Nombre:             string;
    ApellidoPaterno:    string;
    ApellidoMaterno:    string;
    RFC:                string;
    FechaNacimiento:    Date;
    UsuarioAgrega:      number;
}
