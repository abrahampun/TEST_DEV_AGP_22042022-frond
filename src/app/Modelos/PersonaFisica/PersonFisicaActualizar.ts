import { PersonaFisicaCrear } from "./PersonFisicaCrear";

export interface PersonaFisicaActualizar extends PersonaFisicaCrear {
    IdPersonaFisica: number;
}
