export interface PersonaFisica {
    idPersonaFisica:    number;
    fechaRegistro:      Date;
    fechaActualizacion: null;
    nombre:             string;
    apellidoPaterno:    string;
    apellidoMaterno:    string;
    rfc:                string;
    fechaNacimiento:    Date;
    usuarioAgrega:      number;
    activo:             boolean;
}
