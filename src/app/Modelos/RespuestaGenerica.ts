export interface RespuestaGenerica {
    mensajeerror:    string;
    error:      number;

}
export interface RespuestaErrorValidacionGenerica {
    type:    string;
    title:   string;
    status:  number;
    traceId: string;
    errors:  Array<Array<string>>;
}