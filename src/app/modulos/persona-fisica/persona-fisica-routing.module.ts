import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PersonaFisicaComponent } from './persona-fisica.component';
import { CrearComponent } from './vistas/crear/crear.component';
import { ActualizarComponent } from './vistas/actualizar/actualizar.component';
import { ReporteComponent } from './vistas/reporte/reporte.component';

const routes: Routes = [
  { path: '', component: PersonaFisicaComponent },
  { path: 'crear', component: CrearComponent },
  { path: 'actualizar/:id', component: ActualizarComponent },
  { path: 'reporte', component: ReporteComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PersonaFisicaRoutingModule { }
