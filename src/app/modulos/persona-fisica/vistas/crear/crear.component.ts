import { compileDeclareDirectiveFromMetadata } from '@angular/compiler';
import { Component, OnInit, ViewChild } from '@angular/core';
import { observable } from 'rxjs';
import { RespuestaErrorValidacionGenerica, RespuestaGenerica } from 'src/app/Modelos/RespuestaGenerica';
import { PersonasFisicasService } from 'src/app/servicios/personas-fisicas/personas-fisicas.service';
import Swal from 'sweetalert2'
import { FormularioComponent } from '../../componentes/formulario/formulario.component';

@Component({
  selector: 'app-crear',
  templateUrl: './crear.component.html',
  styleUrls: ['./crear.component.scss']
})
export class CrearComponent implements OnInit {
  @ViewChild(FormularioComponent) hijo!: FormularioComponent;
  constructor(private PersonaFisicaServicio: PersonasFisicasService) { }

  ngOnInit(): void {
  }
  ObtenerDatosFormulario(e:any){
    console.log(e)
    this.PersonaFisicaServicio.Guardar(e).subscribe(
      {
        next: (v:RespuestaGenerica ) => {
          
          this.SweetCorrecto()
        },
        error: (e) => {
          if (e.error.mensajeerror) {
            this.SweetError(e.error.mensajeerror);
          }
          else if ( e.error.title) {
          this.SweetErrorValidaciones(e.error.errors)
          }else{
            this.SweetError();
          }
        },
        complete: () => console.info('complete') 
      }
    )
    
  }
  SweetCorrecto(){
    Swal.fire({
      title: 'Personas Fisicas',
      text: 'Registro Correcto',
      icon: 'success',
      confirmButtonText: 'Ok'
    })
    this.hijo.limpiarForm()
  }
  SweetError(text:any = ''){
    if (text) {
      //no hacer nada
    }else{
    text =  "Ocurrio un error, no se logro guarda"
    }
    Swal.fire({
      title: 'Personas Fisicas',
      text: text,
      icon: 'error',
      confirmButtonText: 'Ok'
    })
   //this.hijo.limpiarForm()
  }
  SweetErrorValidaciones(e:any){
    let error;
    //tomamos el primer error
    for (const key in e) {
      if (Object.prototype.hasOwnProperty.call(e, key)) {
         error = e[key][0];
        break
        
      }
    }
    Swal.fire({
      title: 'Personas Fisicas',
      text: error,
      icon: 'error',
      confirmButtonText: 'Ok'
    })
  }
}
