import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { Subject } from 'rxjs';
import { DataContenido, DataReporte } from 'src/app/Modelos/DataReporte';
import { PersonasFisicasService } from 'src/app/servicios/personas-fisicas/personas-fisicas.service';
import Swal from 'sweetalert2'
@Component({
  selector: 'app-reporte',
  templateUrl: './reporte.component.html',
  styleUrls: ['./reporte.component.scss']
})
export class ReporteComponent implements OnInit {
  dtOptions: any = {};
  dtTrigger: Subject<any> = new Subject<any>();
  constructor( private PersonaFisicaServicio: PersonasFisicasService, private cookieServicio: CookieService,private router: Router) { }
  personas: DataContenido[] = [];
  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
       scrollX: true,
       language: {
         processing: "Procesando...",
         lengthMenu: "Mostrar _MENU_ registros",
         zeroRecords: "No se encontraron resultados",
         emptyTable: "Ningún dato disponible en esta tabla",
         infoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
         infoFiltered: "(filtrado de un total de _MAX_ registros)",
         search: "Buscar:",
         loadingRecords: "Cargando...",
         info: "Mostrando _START_ a _END_ de _TOTAL_ registros",
         paginate: {
           first: "Primero",
           last: "Último",
           next: "Siguiente",
           previous: "Anterior"
           },
           
     },
     dom: 'Bfrtip',
     buttons: [
      {
          extend: 'excelHtml5',
          title: 'Reporte Personas'
      },
  ]
    }
   if (!this.cookieServicio.check('userExternal')) {
    this.router.navigate(['/login'])
   }else{

     this.ObtenerTodo()
   }
  }
  ObtenerTodo(){

    this.PersonaFisicaServicio.ObtenerExterno(this.cookieServicio.get('userExternal')).subscribe(
      {
        next: (v) => {
          this.personas = v.Data
          this.dtTrigger.next(null);
        },
        error: (e) => {

          if (e.status == 401) {
            this.cookieServicio.delete('userExternal')
          } else if (e.status == 404) {
            this.SweetError("Registro no encontrado");
          }else{
            this.SweetError("Error al obtener reporte");
          }
     
          this.cookieServicio.delete('userExternal', '/')
          this.cookieServicio.deleteAll('/');
          this.router.navigate(['/login'])
        },
        complete: () => console.info('complete') 
      }
    )
    
  }
  SweetError(text =''){
    if (text) {
    //no hacer nada
  }else{
  text =  "Error"
  }
    Swal.fire({
      title: 'Personas Fisicas',
      text: text,
      icon: 'error',
      confirmButtonText: 'Ok'
    })
  }
}
