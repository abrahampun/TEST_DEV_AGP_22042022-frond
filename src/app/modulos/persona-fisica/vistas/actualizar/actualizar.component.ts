import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute,Params } from '@angular/router';
import { PersonaFisicaActualizar } from 'src/app/Modelos/PersonaFisica/PersonFisicaActualizar';
import { PersonaFisicaCrear } from 'src/app/Modelos/PersonaFisica/PersonFisicaCrear';
import { RespuestaGenerica } from 'src/app/Modelos/RespuestaGenerica';
import { PersonasFisicasService } from 'src/app/servicios/personas-fisicas/personas-fisicas.service';
import Swal from 'sweetalert2'
import { FormularioComponent } from '../../componentes/formulario/formulario.component';
@Component({
  selector: 'app-actualizar',
  templateUrl: './actualizar.component.html',
  styleUrls: ['./actualizar.component.scss']
})
export class ActualizarComponent implements OnInit {
  @ViewChild(FormularioComponent) hijo!: FormularioComponent;
  idPersonaFisica:any ;
  constructor(private PersonaFisicaServicio: PersonasFisicasService, 
                       private rutaActiva: ActivatedRoute,
    ) { }

  ngOnInit(): void {
    const routeParams = this.rutaActiva.snapshot.paramMap;
     this.idPersonaFisica = Number(routeParams.get('id'));

  
  }
  ObtenerDatosFormulario(Datos:PersonaFisicaActualizar ){
    Datos.IdPersonaFisica = this.idPersonaFisica;
    console.log(Datos)
    this.PersonaFisicaServicio.Actualizar(Datos).subscribe(
      {
        next: (v:RespuestaGenerica ) => {
          
          this.SweetCorrecto()
        },
        error: (e) => {
          console.log(e)
        if (e.error.mensajeerror) {
          this.SweetError(e.error.mensajeerror);
          
        }else if ( e.error.title) {
          this.SweetErrorValidaciones(e.error.errors)
          }else{
            this.SweetError();
          }
        },
        complete: () => console.info('complete') 
      }
    )
    
  }
  SweetCorrecto(){
    Swal.fire({
      title: 'Personas Fisicas',
      text: 'Actualizado Correcto',
      icon: 'success',
      confirmButtonText: 'Ok'
    })
   // this.hijo.limpiarForm()
  }
  SweetError(text =''){
    if (text) {
    //no hacer nada
  }else{
  text =  "Ocurrio un error, no se logro actualizar"
  }
    Swal.fire({
      title: 'Personas Fisicas',
      text: text,
      icon: 'error',
      confirmButtonText: 'Ok'
    })
   //this.hijo.limpiarForm()
  }
  SweetErrorValidaciones(e:any){
    let error;
    //tomamos el primer error
    for (const key in e) {
      if (Object.prototype.hasOwnProperty.call(e, key)) {
         error = e[key][0];
       
        break
        
      }
    }
    Swal.fire({
      title: 'Personas Fisicas',
      text: error,
      icon: 'error',
      confirmButtonText: 'Ok'
    })
  }
}
