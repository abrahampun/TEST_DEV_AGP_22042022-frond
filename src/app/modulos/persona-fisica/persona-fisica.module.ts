import { LOCALE_ID, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PersonaFisicaRoutingModule } from './persona-fisica-routing.module';
import { PersonaFisicaComponent } from './persona-fisica.component';
import { CrearComponent } from './vistas/crear/crear.component';
import { ActualizarComponent } from './vistas/actualizar/actualizar.component';
import { FormularioComponent } from './componentes/formulario/formulario.component';
import { DataTablesModule } from "angular-datatables"
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatMomentDateModule, MAT_MOMENT_DATE_ADAPTER_OPTIONS } from '@angular/material-moment-adapter';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ReporteComponent } from './vistas/reporte/reporte.component';


@NgModule({
  declarations: [
    PersonaFisicaComponent,
    CrearComponent,
    ActualizarComponent,
    FormularioComponent,
    ReporteComponent
  ],
  imports: [
    CommonModule,
    PersonaFisicaRoutingModule,
    DataTablesModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatMomentDateModule,
    MatInputModule,
    ReactiveFormsModule,
    FormsModule
    
    
  ],
  providers: [
    {provide: MAT_MOMENT_DATE_ADAPTER_OPTIONS, useValue: {strict: true}},
    { provide: MAT_DATE_LOCALE, useValue: 'es-ES' },
  ]
})
export class PersonaFisicaModule { }
