import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Subject } from 'rxjs/internal/Subject';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { PersonaFisica } from 'src/app/Modelos/PersonaFisica/PersonaFisica';
import { DataTableDirective } from 'angular-datatables/src/angular-datatables.directive';
import { PersonasFisicasService } from 'src/app/servicios/personas-fisicas/personas-fisicas.service';
import Swal from 'sweetalert2'
import * as moment from 'moment';
import { ReturnStatement } from '@angular/compiler';

@Component({
  selector: 'app-persona-fisica',
  templateUrl: './persona-fisica.component.html',
  styleUrls: ['./persona-fisica.component.scss']
})
export class PersonaFisicaComponent implements OnInit, OnDestroy {
  private url = environment.apiUrl;
  constructor(private httpClient: HttpClient, private PersonaFisicaServicio: PersonasFisicasService) { }
 dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject<any>();
  personas: PersonaFisica[] = [];
  @ViewChild(DataTableDirective, { static: false }) 
  private dtElement!: DataTableDirective;
  prueba:any;
  ngOnInit(): void {
    this.dtOptions = {
       pagingType: 'full_numbers',
        scrollX: true,
        language: {
          processing: "Procesando...",
          lengthMenu: "Mostrar _MENU_ registros",
          zeroRecords: "No se encontraron resultados",
          emptyTable: "Ningún dato disponible en esta tabla",
          infoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
          infoFiltered: "(filtrado de un total de _MAX_ registros)",
          search: "Buscar:",
          loadingRecords: "Cargando...",
          info: "Mostrando _START_ a _END_ de _TOTAL_ registros",
          paginate: {
            first: "Primero",
            last: "Último",
            next: "Siguiente",
            previous: "Anterior"
            },
      }
    };
    this.ObtenerTodo()

    //this.dtTrigger.next(true);
  }

ObtenerTodo(){

  this.PersonaFisicaServicio.ObtenerTodo().subscribe(
    {
      next: (v) => {
        this.personas = v;
        this.dtTrigger.next(null);
      },
      error: (e) => {
      if (e.error.ERROR) {
        this.SweetError(e.error.MENSAJEERROR);
      }
        
        if (e.status == 404) {
          this.SweetError("Registro no encontrado");
        }else{
          this.SweetError();
        }
      },
      complete: () => console.info('complete') 
    }
  )
  
}

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next(null);
    });
  }
  SweetCorrecto(){
    Swal.fire({
      title: 'Personas Fisicas',
      text: 'Registro Correcto',
      icon: 'success',
      confirmButtonText: 'Ok'
    })
  }
  SweetError(text =''){
    if (text) {
    //no hacer nada
  }else{
  text =  "Error"
  }
    Swal.fire({
      title: 'Personas Fisicas',
      text: text,
      icon: 'error',
      confirmButtonText: 'Ok'
    })
  }
  formatearFecha(e:any){
 return    moment(e,"YYYY-MM-DD").format('DD/MM/YYYY')
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
}

