import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { PersonaFisicaCrear } from 'src/app/Modelos/PersonaFisica/PersonFisicaCrear';
import * as moment from 'moment';
import { PersonaFisicaActualizar } from 'src/app/Modelos/PersonaFisica/PersonFisicaActualizar';
import Swal from 'sweetalert2'
import { PersonasFisicasService } from 'src/app/servicios/personas-fisicas/personas-fisicas.service';
import { PersonaFisica } from 'src/app/Modelos/PersonaFisica/PersonaFisica';
@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.scss']
})
export class FormularioComponent implements OnInit {
  get Nombre() { return this.form.get('Nombre'); }
  get ApellidoMaterno() { return this.form.get('ApellidoMaterno'); }
  get ApellidoPaterno() { return this.form.get('ApellidoPaterno'); }
  get FechaNacimiento() { return this.form.get('FechaNacimiento'); }
  get RFC() { return this.form.get('RFC'); }
    form:any;
  constructor(private formBuilder: FormBuilder,private PersonaFisicaServicio: PersonasFisicasService) { }
    @Output() MiFormulario = new EventEmitter<any>();
    @Input() id!: number;
    maxDate = new Date;
  ngOnInit(): void {
    this.CrearFormulario()
    if (this.id) {
      this.ObtenerPorId(this.id)
    }
  }
   sub(){
     this.FormatoFecha()
     this.EmitirDatos()

  }
  limpiarForm(){
    this.form.reset();
  }
  EmitirDatos(){
    this.MiFormulario.emit(this.form.value )
  }
  CrearFormulario() {
    this.form = this.formBuilder.group({
      Nombre: ['', [Validators.required]],
      ApellidoMaterno: ['', [Validators.required]],
      ApellidoPaterno: ['', [Validators.required]],
      FechaNacimiento: ['', [Validators.required]],
      RFC: ['', [Validators.required]]

      
    
		});
  }

  letras(event:any) {
		if (!/^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]*$/.test(event.key)) {
			event.preventDefault();
			return false;
		} else {
			return true;
		}
	}
  keyPressAlphaNumeric(event:any) {

    var inp = String.fromCharCode(event.keyCode);

    if (/[a-zA-Z0-9]/.test(inp)) {
      return true;
    } else {
      event.preventDefault();
      return false;
    }
  }
  FormatoFecha(){

    let fechaMoment = this.form.get('FechaNacimiento').value;
    this.form.get('FechaNacimiento').value =  moment(fechaMoment).format('YYYY-MM-DD')

  }

  llenarformulario(datos:PersonaFisica){

    this.form.controls['Nombre'].setValue(datos.nombre);
    this.form.controls['ApellidoMaterno'].setValue(datos.apellidoMaterno);
    this.form.controls['ApellidoPaterno'].setValue(datos.apellidoPaterno);
    

    this.form.controls['FechaNacimiento'].setValue(datos.fechaNacimiento,);
    
    this.form.controls['RFC'].setValue(datos.rfc);
    
  }
  ObtenerPorId(id:number){

    this.PersonaFisicaServicio.ObtenerPorId(id).subscribe(
      {
        next: (v) => {
          this.llenarformulario(v)
         // this.SweetCorrecto()
        },
        error: (e) => {
        if (e.error.ERROR) {
          this.SweetError(e.error.MENSAJEERROR);
        }
          
          if (e.status == 404) {
            this.SweetError("Registro no encontrado");
          }else{
            this.SweetError();
          }
        },
        complete: () => console.info('complete') 
      }
    )
    
  }
  SweetCorrecto(){
    Swal.fire({
      title: 'Personas Fisicas',
      text: 'Actualizado Correcto',
      icon: 'success',
      confirmButtonText: 'Ok'
    })

  }
  SweetError(text =''){
    if (text) {
    //no hacer nada
  }else{
  text =  "Error"
  }
    Swal.fire({
      title: 'Personas Fisicas',
      text: text,
      icon: 'error',
      confirmButtonText: 'Ok'
    })
  }
}
