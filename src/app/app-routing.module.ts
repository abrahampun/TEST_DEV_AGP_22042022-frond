import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './componentes/login/login.component';
import { HttpClientModule } from '@angular/common/http';

const routes: Routes = [
  { path: '', redirectTo:'personas-fisicas', pathMatch: 'full' },
  {
		path: 'personas-fisicas',
		loadChildren: () =>
			import('./modulos/persona-fisica/persona-fisica.module').then(
				(m) => m.PersonaFisicaModule
			),
	},
	{ path: 'login', component: LoginComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes),HttpClientModule],
  exports: [RouterModule,]
})

export class AppRoutingModule { 


  
}
