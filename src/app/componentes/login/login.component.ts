import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Route, Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { LoginService } from 'src/app/servicios/login/login.service';
import Swal from 'sweetalert2'
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form:any;
  constructor( private loginServicio: LoginService,private formBuilder: FormBuilder,private cookieServicio: CookieService, private router: Router) { }

  ngOnInit(): void {
    if ( this.cookieServicio.check('userExternal' )) {
      this.router.navigate(['/personas-fisicas/reporte'])
    }
    this.CrearFormulario();
  }
  CrearFormulario() {
    this.form = this.formBuilder.group({
      Username: ['', [Validators.required]],
      Password: ['', [Validators.required]],
		});
  }
  sub(){

    this.loginServicio.login(this.form.value).subscribe(
      {
        next: (v ) => {
          this.cookieServicio.set('userExternal', v.Data ,{path: '/'});

          this.router.navigate(['/personas-fisicas/reporte'])
        },
        error: (e) => {
          if ( e.status == 401) {
            this.SweetError("Credenciales Incorrectas");
          }else{
            this.SweetError();
          }
        },
        complete: () => console.info('complete') 
      }
    )
    
  }
  SweetError(txt = ""){
    if (txt) {
      
    }else{
      txt = "Error al intentar iniciar sesión"
    }
    Swal.fire({
      title: 'Inicio de sesión',
      text: txt,
      icon: 'error',
      confirmButtonText: 'Ok'
    })

  }

}
